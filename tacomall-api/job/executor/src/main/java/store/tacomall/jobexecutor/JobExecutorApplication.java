/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2021-10-10 16:45:46
 * @LastEditTime: 2021-10-15 16:51:36
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/job/executor/src/java/store/tacomall/jobexecutor/JobExecutorApplication.java
 */
package store.tacomall.jobexecutor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobExecutorApplication {

  public static void main(String[] args) {
    SpringApplication.run(JobExecutorApplication.class, args);
  }

}